package io.SeleniumDemo;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;

@SpringBootTest
class SeleniumDemoApplicationTests {

	@BeforeClass
	void init_global(){
	}

	@Before
	void init(){
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\andi\\Downloads\\chromedriver_win32\\chromedriver.exe");
	}

	@Test
	void Searching_google_test() {

		String keyWord = "tokopedia";
		String url = "https://www.google.com/webhp?authuser=1";
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\andi\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		//open website
		driver.get(url);

		//select element
		WebElement searchBox = driver.findElement(By.name("q"));
		searchBox.sendKeys(keyWord);
		searchBox.submit();

		
	}

	@Test
	void Searching_tokopedia_test() {

		String keyWord = "liquid";
		String url = "https://www.tokopedia.com/";
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\andi\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		//open website
		driver.get(url);

		//select element
		WebElement divSearchBox = driver.findElement(By.tagName("div").className("css-rl8xd2").tagName("input"));
		divSearchBox.sendKeys(keyWord);
		divSearchBox.submit();
		
	}

	@Test
	void Searching_youtube_test() {

		String keyWord = "majelis lucu indonesia";
		String url = "https://www.youtube.com/?hl=id&gl=ID";
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\andi\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		//open website
		driver.get(url);

		//select element
		List<HashMap<String,String>> videos = new ArrayList();
		for(WebElement e : driver.findElements(By.tagName("ytd-rich-item-renderer"))){

			WebElement data = e.findElement(By.id("video-title-link"));
			
			HashMap<String,String> dataVideo = new HashMap<>(){{
				put("Title", data.getAttribute("title"));
				put("Link", data.getAttribute("href"));
			}};

			videos.add(dataVideo);

		}



		String dbg = "";
		
	}

	@Test
	void Searching_youtube_and_open_all_video_test() {

		String keyWord = "majelis lucu indonesia";
		String url = "https://www.youtube.com/?hl=id&gl=ID";
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\andi\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		//open website
		driver.get(url);
		// String lk = "https://stackoverflow.com/questions/15558482/javascript-syntaxerror-missing-after-argument-list";
		// String script2 = "window.open('"+lk+"')";
		// ((JavascriptExecutor) driver).executeScript(script2);
		//select element

		WebElement searchBox = driver.findElement(By.tagName("input"));
		searchBox.sendKeys(keyWord);
		searchBox.submit();
		List<String> link = new ArrayList();
		for(WebElement e : driver.findElements(By.tagName("ytd-video-renderer"))){

			WebElement anchorVideo = e.findElement(By.id("video-title"));
			String linkVideo = anchorVideo.getAttribute("href");
			String script = "window.open('"+linkVideo+"')";
			((JavascriptExecutor) driver).executeScript(script);
			link.add(linkVideo);
		}

		

		String dbg = "";
		
	}

}
